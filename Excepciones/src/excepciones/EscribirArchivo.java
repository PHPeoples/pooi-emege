package excepciones;

import java.io.BufferedWriter;
import java.io.FileWriter;

public class EscribirArchivo {
	
	public static void main(String[] args) {
		String archivo = "src/excepciones/data/prueba";
		BufferedWriter bw = null;

		try {
			FileWriter fw = new FileWriter(archivo);
			bw = new BufferedWriter(fw);
			
			bw.write("Archivo de prueba");
			bw.newLine();
			bw.write("Nueva línea");
			bw.newLine();
		} catch (Exception e) {
			System.out.println(e);
		} finally {
			try {
				if (null != bw) {
					bw.close();
				}
			} catch (Exception e2) {
				System.out.println(e2);
			}
		}
	}
}
