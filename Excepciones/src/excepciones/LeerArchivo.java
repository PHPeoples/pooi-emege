package excepciones;

import java.io.BufferedReader;
import java.io.FileReader;

public class LeerArchivo {
	
	public static void main(String[] args) {
		String archivo = "src/excepciones/data/prueba";
		BufferedReader br = null;

		try {
			br = new BufferedReader(new FileReader(archivo));

			String linea;
			while ((linea = br.readLine()) != null)
				System.out.println(linea);
		} catch (Exception e) {
			System.out.println(e);
		} finally {
			try {
				if (null != br) {
					br.close();
				}
			} catch (Exception e2) {
				System.out.println(e2);
			}
		}
	}
}
