package ej1;

public class Main {

	public static void main(String[] args) {
		try {
			Exception e = new Exception("Prueba");
			throw new Exception(e);
		} catch (Exception exception) {
			System.out.println(exception.getMessage());
		} finally {
			System.out.println("Finally");
		}
	}

}
