package figura;

import punto.Punto2D;

public class Main {

	public static void main(String[] args) {
		Circulo c1 = new Circulo("rojo", new Punto2D(1, 1), 3.0);
		System.out.println(c1);
		System.out.println(c1.getArea());
	}

}
