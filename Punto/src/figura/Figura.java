package figura;

import punto.Punto;

public abstract class Figura {
	private String color;
	private Punto origen;
	
	public Figura(String color, Punto origen) {
		this.setColor(color);
		this.setOrigen(origen);
	}

	public abstract Double getArea();

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public Punto getOrigen() {
		return origen;
	}

	public void setOrigen(Punto puntoOrigen) {
		this.origen = puntoOrigen;
	}
	
	public void desplazar(Punto p) {
		this.setOrigen((Punto) this.getOrigen().desplazamiento(p));
	}
}
