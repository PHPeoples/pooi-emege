package figura;

import punto.Punto;

public class Circulo extends Figura {
	private Double radio;
	
	public Circulo(String color, Punto origen, Double radio) {
		super(color, origen);
		this.setRadio(radio);
	}

	public Double getRadio() {
		return radio;
	}

	public void setRadio(Double radio) {
		this.radio = radio;
	}

	@Override
	public Double getArea() {
		return Math.PI * this.getRadio();
	}
	
	@Override
	public String toString() {
		return "Circulo " + this.getColor() + ". Radio: " + this.getRadio();
	}
}
