package colecciones;

import java.util.ArrayList;

import punto.Punto2D;

public class Plano {
	private ArrayList<Punto2D> puntos;
	
	public Plano() {
		this.setPuntos(new ArrayList<Punto2D>());
	}

	public ArrayList<Punto2D> getPuntos() {
		return puntos;
	}

	public void setPuntos(ArrayList<Punto2D> puntos) {
		this.puntos = puntos;
	}
	
	public void agregarPunto(Punto2D p) {
		this.getPuntos().add(p);
	}
	
	public Integer getCantPuntos(Cuadrante cuadrante) {
		Integer contador = 0;
		
		for (Punto2D p : this.getPuntos()) {
			if (p.getCuadrante() == cuadrante) contador ++;
		}
		
		return contador;
	}
}
