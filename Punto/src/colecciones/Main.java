package colecciones;

import punto.Punto2D;

public class Main {

	public static void main(String[] args) {
		Plano plano = new Plano();
		plano.agregarPunto(new Punto2D());
		plano.agregarPunto(new Punto2D(-1));
		plano.agregarPunto(new Punto2D(1));
		plano.agregarPunto(new Punto2D(-1, 1));
		plano.agregarPunto(new Punto2D(1, -1));
		plano.agregarPunto(new Punto2D(-2, 3));
		plano.agregarPunto(new Punto2D(0, 3));
		
		for (Cuadrante c : Cuadrante.values()) {
			System.out.println("Puntos en el cuadrante '" + c + "': " + plano.getCantPuntos(c));
		}
	}

}
