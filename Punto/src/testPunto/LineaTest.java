package testPunto;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import punto.Linea;
import punto.Punto2D;

class LineaTest {

	@Test
	void test() {
		Punto2D punto1 = new Punto2D();
		Punto2D punto2 = new Punto2D(1, 1);
		Linea linea = new Linea(punto1, punto2);
		
		assertEquals(linea.getLongitudLinea(), punto1.calcularDistancia(punto2));
	}

}
