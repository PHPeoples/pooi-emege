package testPunto;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import punto.Punto2D;
import punto.Punto3D;

class Punto3DTest {

	@Test
	void test() {
		Punto2D p1 = new Punto3D(1, 2, 3);
		Punto3D p2 = new Punto3D(4, 8, 5);
		
		Double res1 = 7.000000000000001;
		
		assertEquals(res1, p1.calcularDistancia(p2));
	}

}
