package punto;

public interface Punto {
	public Double calcularDistancia(Object obj);
	public Double distanciaAlOrigen();
	public String toString();
	public boolean equals(Object obj);
	public Object clone();
	public Object desplazamiento(Object obj);
}
