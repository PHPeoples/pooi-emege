package punto;

public class Punto3D extends Punto2D {
	private Integer z;
	
	public Punto3D(Integer x, Integer y, Integer z) {
		super(x, y);
		this.setZ(z);
	}
	
	public Punto3D(Integer xyz) {
		this(xyz, xyz, xyz);
	}
	
	public Punto3D() {
		this(0);
	}

	public Integer getZ() {
		return z;
	}

	public void setZ(Integer z) {
		this.z = z;
	}
	
	@Override
	public Double calcularDistancia(Object obj) {
		if (obj == null) return null;
		if (!(obj instanceof Punto3D)) return null;
		
		Punto3D punto3d = (Punto3D) obj;
		
		Double catetoX = Math.pow(punto3d.getX() - this.getX(), 2);
		Double catetoY = Math.pow(punto3d.getY() - this.getY(), 2);
		Double catetoZ = Math.pow(punto3d.getZ() - this.getZ(), 2);
		return Math.sqrt(catetoX + catetoY + catetoZ);
	}
	
	@Override
	public Double distanciaAlOrigen() {
		return this.calcularDistancia(new Punto3D());
	}
	
	@Override
	public String toString() {
		return super.toString() + " " + this.getZ();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Punto3D other = (Punto3D) obj;
		if (z == null) {
			if (other.z != null)
				return false;
		} else if (!z.equals(other.z))
			return false;
		return true;
	}
	
	@Override
	public Punto3D clone() {
		return new Punto3D(this.getX(), this.getY(), this.getZ());
	}
	
	@Override
	public Punto3D desplazamiento(Object obj) {
		Punto3D punto = (Punto3D) obj;
		
		Integer x = this.getX() +  punto.getX();
		Integer y = this.getY() +  punto.getY();
		Integer z = this.getZ() +  punto.getZ();
		
		return new Punto3D(x, y, z);
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((z == null) ? 0 : z.hashCode());
		return result;
	}
}
