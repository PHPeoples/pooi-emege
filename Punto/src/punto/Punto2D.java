package punto;

import colecciones.Cuadrante;

public class Punto2D implements Punto {
	private Integer x;
	private Integer y;
	
	public Punto2D(Integer x, Integer y) {
		this.setX(x);
		this.setY(y);
	}
	
	public Punto2D(Integer xy) {
		this(xy, xy);
	}
	
	public Punto2D() {
		this(0);
	}
	
	public Integer getX() {
		return x;
	}
	
	public void setX(Integer x) {
		this.x = x;
	}
	
	public Integer getY() {
		return y;
	}
	
	public void setY(Integer y) {
		this.y = y;
	}
	
	public Double calcularDistancia(Object obj) {
		Punto2D punto = (Punto2D) obj;
		
		Double cateto1 = Math.pow((punto.getX() - this.getX()) , 2);
		Double cateto2 = Math.pow((punto.getY() - this.getY()) , 2);

		return Math.sqrt(cateto1 + cateto2);
	}
	
	public Double distanciaAlOrigen() {
		return this.calcularDistancia(new Punto2D());
	}
	
	@Override
	public String toString() {
		return this.getX() + " " + this.getY();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Punto2D other = (Punto2D) obj;
		if (x == null) {
			if (other.x != null)
				return false;
		} else if (!x.equals(other.x))
			return false;
		if (y == null) {
			if (other.y != null)
				return false;
		} else if (!y.equals(other.y))
			return false;
		return true;
	}
	
	@Override
	public Object clone() {
		return new Punto2D(this.getX(), this.getY());
	}
	
	public Punto2D desplazamiento(Object obj) {
		Punto2D punto = (Punto2D) obj;
		
		Integer x = this.getX() +  punto.getX();
		Integer y = this.getY() +  punto.getY();
		
		return new Punto2D(x, y);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((x == null) ? 0 : x.hashCode());
		result = prime * result + ((y == null) ? 0 : y.hashCode());
		return result;
	}
	
	public Cuadrante getCuadrante() {
		if (this.getX() > 0 && this.getY() > 0) return Cuadrante.PRIMER;
		if (this.getX() < 0 && this.getY() > 0) return Cuadrante.SEGUNDO;
		if (this.getX() < 0 && this.getY() < 0) return Cuadrante.TERCER;
		if (this.getX() > 0 && this.getY() < 0) return Cuadrante.CUARTO;
		
		return Cuadrante.SIN_CUADRANTE;
	}
}
