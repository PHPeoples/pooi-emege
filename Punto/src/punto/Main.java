package punto;

public class Main {

	public static void main(String[] args) {
		Punto2D p1 = new Punto2D(2, 3);
		Punto2D p2 = new Punto3D(1, 2, 3);
		Punto3D p3 = new Punto3D(4, 8, 5);
		
		System.out.println(p1);
		System.out.println(p2);
		System.out.println(p3);
	}

}
