package composite;

public class Circulo implements Figura {
	private Double radio;

	public Circulo(Double radio) {
		this.setRadio(radio);
	}

	public Double getRadio() {
		return radio;
	}

	public void setRadio(Double radio) {
		this.radio = radio;
	}

	@Override
	public Double calcularArea() {
		return Math.PI * Math.pow(this.getRadio(), 2);
	}

}
