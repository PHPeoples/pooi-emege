package composite;

import java.util.ArrayList;

public class FiguraCompuesta implements Figura {
	private ArrayList<Figura> figuras;
	
	public FiguraCompuesta() {
		this.figuras = new ArrayList<Figura>();
	}
	
	public void agregar(Figura f) {
		this.figuras.add(f);
	}

	@Override
	public Double calcularArea() {
		Double area = 0.0;
		
		for (Figura figura : this.figuras) {
			area += figura.calcularArea();
		}
		
		return area;
	}

}
