package composite;

public interface Figura {
	public Double calcularArea();
}
