package composite;

public class Rectangulo implements Figura {
	private Double b;
	private Double h;
	
	public Rectangulo(Double b, Double h) {
		this.setB(b);
		this.setH(h);
	}

	public Double getB() {
		return b;
	}

	public void setB(Double b) {
		this.b = b;
	}

	public Double getH() {
		return h;
	}

	public void setH(Double h) {
		this.h = h;
	}

	@Override
	public Double calcularArea() {
		return this.getB() * this.getH();
	}

}
