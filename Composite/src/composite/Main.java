package composite;

public class Main {

	public static void main(String[] args) {
		Circulo rueda = new Circulo(1.0);
		Triangulo frente = new Triangulo(1.0, 2.0);
		Rectangulo chasisChico = new Rectangulo(1.0, 2.0);
		Rectangulo chasisGrande = new Rectangulo(2.0, 2.0);
		Rectangulo chimenea = new Rectangulo(1.0, 1.0);
		
		FiguraCompuesta locomotora = new FiguraCompuesta();
		locomotora.agregar(rueda);
		locomotora.agregar(rueda);
		locomotora.agregar(rueda);
		locomotora.agregar(frente);
		locomotora.agregar(chasisChico);
		locomotora.agregar(chimenea);
		
		FiguraCompuesta vagon = new FiguraCompuesta();
		vagon.agregar(rueda);
		vagon.agregar(rueda);
		vagon.agregar(chasisGrande);
		
		FiguraCompuesta tren = new FiguraCompuesta();
		tren.agregar(locomotora);
		tren.agregar(vagon);
		tren.agregar(vagon);
		
		System.out.println(tren.calcularArea());
	}

}
