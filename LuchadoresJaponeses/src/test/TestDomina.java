package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;

import luchadoresJaponeses.Luchador;

class TestDomina {

	@AfterEach
	void tearDown() throws Exception {
	}

	@org.junit.jupiter.api.Test
	void test1() {
		Luchador l1 = new Luchador(300, 1500);
		Luchador l2 = new Luchador(250, 1400);
		
		assertEquals(true, l1.domina(l2));
	}
	
	@org.junit.jupiter.api.Test
	void test2() {
		Luchador l1 = new Luchador(300, 1500);
		Luchador l2 = new Luchador(250, 1400);
		
		assertEquals(false, l2.domina(l1));
	}
	
	@org.junit.jupiter.api.Test
	void test3() {
		Luchador l1 = new Luchador(300, 1500);
		Luchador l2 = new Luchador(300, 1400);
		
		assertEquals(true, l1.domina(l2));
	}
	
	@org.junit.jupiter.api.Test
	void test4() {
		Luchador l1 = new Luchador(300, 1500);
		Luchador l2 = new Luchador(300, 1400);
		
		assertEquals(false, l2.domina(l1));
	}
	
	@org.junit.jupiter.api.Test
	void test5() {
		Luchador l1 = new Luchador(350, 1500);
		Luchador l2 = new Luchador(300, 1500);
		
		assertEquals(true, l1.domina(l2));
	}
	
	@org.junit.jupiter.api.Test
	void test6() {
		Luchador l1 = new Luchador(350, 1500);
		Luchador l2 = new Luchador(300, 1500);
		
		assertEquals(false, l2.domina(l1));
	}

}
