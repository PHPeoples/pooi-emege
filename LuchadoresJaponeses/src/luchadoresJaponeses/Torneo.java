package luchadoresJaponeses;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Scanner;

public class Torneo {
	private ArrayList<Luchador> luchadores;
	private ArrayList<Integer> resultados;
	
	public Torneo (String archivo) throws FileNotFoundException {
		this.resultados = null;
		this.luchadores = this.cargarLuchadores(archivo);
	}
	
	public ArrayList<Luchador> getLuchadores() {
		return this.luchadores;
	}
	
	public ArrayList<Integer> getResultados() {
		return this.resultados;
	}
	
	private ArrayList<Luchador> cargarLuchadores(String archivo) throws FileNotFoundException {
		ArrayList<Luchador> luchadores = new ArrayList<>();
		
		Scanner sc = new Scanner(new File(archivo));
		sc.useLocale(Locale.ENGLISH);
		
		while (sc.hasNext()) {
			Luchador l = new Luchador(sc.nextInt(), sc.nextInt());
			luchadores.add(l);
		}
		
		sc.close();
		
		return luchadores;
	}
	
	public void resolver() {
		ArrayList<Integer> r = new ArrayList<>();
		Integer domina;
		
		for (Luchador luchador : this.luchadores) {
			domina = 0;
			
			for (Luchador rival : this.luchadores) {
				if (luchador.domina(rival)) domina ++;
			}
			
			r.add(domina);
		}
		
		this.resultados = r;
	}
	
	public void escribirResultados(String archivo) throws IOException {
		if (this.resultados != null) {
			PrintWriter salida = new PrintWriter(new FileWriter(archivo));
			
			for (Integer resultado : this.resultados) {
				salida.println(resultado);
			}
			
			salida.close();
		}
	}
}
