package luchadoresJaponeses;

import java.io.IOException;

public class Main {

	public static void main(String[] args) throws IOException {
		Torneo t = new Torneo("src/data/luchadores.in");
		t.resolver();
		t.escribirResultados("src/data/sumo.out");
	}

}
