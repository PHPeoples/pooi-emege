package luchadoresJaponeses;

public class Luchador {
	private Integer p;
	private Integer h;
	
	public Luchador(Integer p, Integer h) {
		this.setP(p);
		this.setH(h);
	}
	
	public Integer getP() {
		return p;
	}
	
	public void setP(Integer p) {
		this.p = p;
	}
	
	public Integer getH() {
		return h;
	}
	
	public void setH(Integer h) {
		this.h = h;
	}
	
	public Boolean domina (Luchador l) {
		if (this.getH() > l.getH() && this.getP() > l.getP()) return true;
		if (this.getP().equals(l.getP()) && this.getH() > l.getH()) return true;
		if (this.getH().equals(l.getH()) && this.getP() > l.getP()) return true;
		
		return false;
	}
	
	@Override
	public String toString() {
		return this.getP() + " " + this.getH();
	}
}
