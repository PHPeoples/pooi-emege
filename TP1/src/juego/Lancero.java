package juego;

public class Lancero extends Unidad {

	public Lancero(Punto ubicacion) {
		super(25, 150, ubicacion, 1.0, 3.0);
	}

	@Override
	public Boolean atacar(Unidad rival) {
		//Valida salud
		if(this.getSalud() <= 0) return false;
		
		//Valida distancia
		Double distancia = this.getUbicacion().calcularDistancia(rival.getUbicacion());
		if(distancia < this.getDistanciaMinAtaque() || distancia > this.getDistanciaMaxAtaque()) return false;
		
		//Actualiza salud del rival
		Integer salud;
		salud = rival.getSalud() - this.getDanio();
		if(salud < 0) salud = 0;
		rival.setSalud(salud);
		
		return true;
	}
	
	
	

}
