package juego;

public abstract class Objeto {
	private Boolean utilizado;

	public Objeto() {
		this.setUtilizado(false);
	}

	public Boolean getUtilizado() {
		return utilizado;
	}

	public void setUtilizado(Boolean utilizado) {
		this.utilizado = utilizado;
	}
}
