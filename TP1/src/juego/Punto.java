package juego;

public class Punto {
	private Integer x;
	private Integer y;
	
	public Punto(Integer x, Integer y) {
		this.setX(x);
		this.setY(y);
	}
	
	public Punto(Integer xy) {
		this(xy, xy);
	}
	
	public Punto() {
		this(0);
	}
	
	public Integer getX() {
		return x;
	}
	
	public void setX(Integer x) {
		this.x = x;
	}
	
	public Integer getY() {
		return y;
	}
	
	public void setY(Integer y) {
		this.y = y;
	}
	
	public Double calcularDistancia(Punto punto) {
		Double cateto1 = Math.pow((punto.getX() - this.getX()) , 2);
		Double cateto2 = Math.pow((punto.getY() - this.getY()) , 2);

		return Math.sqrt(cateto1 + cateto2);
	}
}
