package juego;

public abstract class Unidad {
	private Integer danio;
	private Integer salud;
	private Punto ubicacion;
	private Double distanciaMinAtaque; 
	private Double distanciaMaxAtaque;
	
	public Unidad(Integer danio, Integer salud, Punto ubicacion, Double distanciaMinAtaque, Double distanciaMaxAtaque) {
		this.setDanio(danio);
		this.setSalud(salud);
		this.setUbicacion(ubicacion);
		this.setDistanciaMinAtaque(distanciaMinAtaque);
		this.setDistanciaMaxAtaque(distanciaMaxAtaque);
	}

	public Integer getDanio() {
		return danio;
	}
	
	private void setDanio(Integer danio) {
		this.danio = danio;
	}
	
	public Integer getSalud() {
		return salud;
	}
	
	protected void setSalud(Integer salud) {
		this.salud = salud;
	}
	
	public Punto getUbicacion() {
		return ubicacion;
	}
	
	private void setUbicacion(Punto ubicacion) {
		this.ubicacion = ubicacion;
	}
	
	public Double getDistanciaMinAtaque() {
		return distanciaMinAtaque;
	}
	
	private void setDistanciaMinAtaque(Double distanciaMinAtaque) {
		this.distanciaMinAtaque = distanciaMinAtaque;
	}
	
	public Double getDistanciaMaxAtaque() {
		return distanciaMaxAtaque;
	}
	
	private void setDistanciaMaxAtaque(Double distanciaMaxAtaque) {
		this.distanciaMaxAtaque = distanciaMaxAtaque;
	}
	
	public abstract Boolean atacar(Unidad rival);
}
