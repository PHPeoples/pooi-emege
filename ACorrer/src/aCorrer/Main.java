package aCorrer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Main {

	public static void main(String[] args) {
		try {
			FileReader input = new FileReader(new File("src/aCorrer/data/carrera.in"));
			BufferedReader bufInput = new BufferedReader(input);
			String line;
			Integer registro = 1;
			Integer competidores[];
			Integer cf[] = null;
			Integer iCf = 0;
			Integer cm[] = null;
			Integer arribaron[] = null;

			line = bufInput.readLine();

			while (line != null) {
				if (registro == 1) {
					String[] datos;
					datos = line.split(" ");
					
					competidores = new Integer[Integer.parseInt(datos[0])];
					cf = new Integer[Integer.parseInt(datos[1])];
					cm = new Integer[Integer.parseInt(datos[2])];
					arribaron = new Integer[Integer.parseInt(datos[3])];
				}
				
				if (registro > 2 && registro < cf.length) {
					String[] datos;
					datos = line.split(" ");
					
					cf[iCf] = Integer.parseInt(datos[0]);
					
					iCf ++;
				}
				
				String[] datos;
				datos = line.split(" ");
				
				int entero1 = Integer.parseInt(datos[0]);
				int entero2 = Integer.parseInt(datos[1]);
				if (datos.length == 3) {
					int entero3 = Integer.parseInt(datos[2]);
					System.out.println("" + entero1 + " " + entero2 + " " + entero3);
				} else {
					System.out.println("" + entero1 + " " + entero2);
				}

				line = bufInput.readLine();
				registro ++;
			}
			bufInput.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
