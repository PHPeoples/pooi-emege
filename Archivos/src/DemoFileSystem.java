import java.io.File;
import java.io.IOException;
import java.util.Date;

public class DemoFileSystem {
	public static void main(String[] args) throws IOException {
		if (args.length > 0) {
			if (args[0] != null) {
				File f = new File(args[0]);
				
				if (f.isDirectory()) {
					for (String string : f.list(new MiFiltro(new Date(0), ".txt"))) {
						System.out.println(string);
					}
				}
			}
		}
	}
}
