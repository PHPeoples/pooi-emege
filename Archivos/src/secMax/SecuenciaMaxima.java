package secMax;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;

public class SecuenciaMaxima {
	
	public static void main(String[] arg) {
		Integer valor;
		Integer validos = 0;
		Integer sec = 0;
		Integer secMax = 0;
		String path = "src/secMax/data/";
		
		File f = null;
		FileReader fr = null;
		BufferedReader br = null;
		
		FileWriter fw = null;
		PrintWriter pw = null;

		try {
			f = new File(path + "SECMAX.IN");
			fr = new FileReader(f);
			br = new BufferedReader(fr);
			String linea;

			while ((linea = br.readLine()) != null) {
				valor = Integer.parseInt(linea);
				
				if (valor % 2 == 0 || valor % 3 == 0 || valor % 5 == 0) {
					if (sec > secMax) secMax = sec;
					sec = 0;
				} else {
					sec ++;
					validos ++;
				}
			}
			
			if (sec > secMax) secMax = sec;
			
			fw = new FileWriter(path + "SECMAX.OUT");
			pw = new PrintWriter(fw);
			
			pw.println(validos);
			pw.println(secMax);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				// Nuevamente aprovechamos el finally para
				// asegurarnos que se cierra el fichero.
				if (null != pw)
					pw.close();
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
	}
}
