package io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Scanner;

public class LeerPersonas {
	
	public static void main(String[] args) throws IOException {
		ArrayList<Persona> lista = getPersonas("src/io/data/personas.in");
		escribirOrdenado(lista, "src/io/data/personasOrdenadas.out");
		escribirOrdenado(mayoresDeEdad(lista, 30), "src/io/data/personasMayoresOrdenadas.out");
	}

	public static ArrayList<Persona> getPersonas(String miArchivo) throws FileNotFoundException{
		ArrayList<Persona> personas = new ArrayList<>();
		
		Scanner sc = new Scanner(new File(miArchivo));
		sc.useLocale(Locale.ENGLISH);
		
		while (sc.hasNext()) {
			Persona p = new Persona(sc.nextInt(), sc.next(), sc.nextInt());
			personas.add(p);
		}
		
		sc.close();
		
		return personas;
	}
	
	public static ArrayList<Persona> mayoresDeEdad(ArrayList<Persona> L, Integer edad) {
		ArrayList<Persona> mayores = new ArrayList<>();
		
		for (Persona persona : L) {
			if (persona.getEdad() >= edad)
				mayores.add(persona);
		}
		
		return mayores;
	}
	
	public static void escribirOrdenado(ArrayList<Persona> lista, String archivo) throws IOException {
		ArrayList<Persona> L = ordenarLista(lista);

		PrintWriter salida = new PrintWriter(new FileWriter(archivo));
		
		for (Persona persona : L) {
			salida.println(persona);
		}
		
		salida.close();
	}
	
	public static ArrayList<Persona> ordenarLista (ArrayList<Persona> L) {
		L.sort(null);
		
		return L;
	}
}
