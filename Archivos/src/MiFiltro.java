import java.io.File;
import java.io.FilenameFilter;
import java.util.Date;

public class MiFiltro implements FilenameFilter {
	private Date date;
	private String extension;

	public MiFiltro(Date date, String extension) {
		this.setDate(date);
		this.setExtension(extension);
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	@Override
	public boolean accept(File dir, String name) {
		return name.endsWith(this.getExtension()) && 
				dir.lastModified() > this.getDate().getTime();
	}

}
