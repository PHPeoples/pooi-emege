package prueba;

public abstract class Unidad {
	private Integer danio;
	
	public Unidad(Integer danio) {
		this.setDanio(danio);
	}

	public Integer getDanio() {
		return danio;
	}

	private void setDanio(Integer danio) {
		this.danio = danio;
	}
}
