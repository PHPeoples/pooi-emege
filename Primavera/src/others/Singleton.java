package others;

public class Singleton {
	private static Singleton instancia;
	
	private Singleton() {}
	
	public static Singleton getInstancia() {
		if (instancia == null)
			Singleton.setInstancia(new Singleton());
		
		return instancia;
	}
	
	private static void setInstancia(Singleton singleton) {
		instancia = singleton;
	}
}
