import java.util.Date;

public class Concreta extends Abstracta implements Interfazeable {
	private boolean booleano;
	private int entero;
	private Date date;
	
	public Concreta(boolean booleano, int entero, Date date) {
		this.setBooleano(booleano);
		this.setEntero(entero);
		this.setDate(date);
	}
	
	public Concreta() {
		this(false, 0, null);
	}
	
	public Concreta(boolean booleano) {
		this(booleano, 0, null);
	}
	
	public Concreta(int entero) {
		this(false, entero, null);
	}
	
	public Concreta(Date date) {
		this(false, 0, date);
	}
	
	public Concreta(boolean booleano, int entero) {
		this(booleano, entero, null);
	}
	
	public Concreta(boolean booleano, Date date) {
		this(booleano, 0, date);
	}
	
	public Concreta(int entero, Date date) {
		this(false, entero, date);
	}
	
	public boolean isBooleano() {
		return booleano;
	}
	
	public void setBooleano(boolean booleano) {
		this.booleano = booleano;
	}
	
	public int getEntero() {
		return entero;
	}
	
	public void setEntero(int entero) {
		this.entero = entero;
	}
	
	public Date getDate() {
		return date;
	}
	
	public void setDate(Date date) {
		this.date = date;
	}
	
	public void metodoPublico() {
		this.metodoPrivado1();
		this.metodoPrivado2();
		this.metodoPrivado3();
	}
	
	private void metodoPrivado1() {}
	private void metodoPrivado2() {}
	private void metodoPrivado3() {}

	@Override
	public void metodoAbstracto() {
		// TODO Auto-generated method stub
	}

	@Override
	public void metodoInterfaz1() {
		// TODO Auto-generated method stub	
	}

	@Override
	public void metodoInterfaz2() {
		// TODO Auto-generated method stub
	}
}
