package genericidad;

public class Punto<T1, T2> {
	private T1 x;
	private T2 y;
	
	
	public Punto(T1 x, T2 y) {
		super();
		this.x = x;
		this.y = y;
	}
	
	public Punto() {
		this(null, null);
	}
	
	public T1 getX() {
		return x;
	}
	
	public void setX(T1 x) {
		this.x = x;
	}
	
	public T2 getY() {
		return y;
	}
	
	public void setY(T2 y) {
		this.y = y;
	}
	
	@Override
	public String toString() {
		return this.getX() + ";" + this.getY();
	}
}
