package genericidad;

public class Main {

	public static void main(String[] args) {
		Punto<Integer, Double> p = new Punto<Integer, Double>(1, 1.0);
		System.out.println(p);
		
		Punto<Punto, Punto> p1 = new Punto<Punto, Punto>(p, new Punto());
		System.out.println(p1);
	}

}
