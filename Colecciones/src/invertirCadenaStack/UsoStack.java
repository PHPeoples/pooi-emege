package invertirCadenaStack;

import java.util.Stack;

public class UsoStack {
	public static String invertirCadena(String cadena) {
		Stack<Character> stack = new Stack<>();
		String inverso = "";
		
		for (int i = 0; i < cadena.length(); i++)
			stack.push(cadena.charAt(i));
		
		while (!stack.isEmpty())
			inverso += stack.pop();
		
		return inverso;
	}
}
