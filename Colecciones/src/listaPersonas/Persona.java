package listaPersonas;

public class Persona implements Comparable<Persona> {
	private Integer dni;
	private String apellido;
	private Integer edad;
	
	public Persona(Integer dni, String apellido, Integer edad) {
		this.setDni(dni);
		this.setEdad(edad);
		this.setApellido(apellido);
	}
	
	public Integer getDni() {
		return dni;
	}
	
	public void setDni(Integer dni) {
		this.dni = dni;
	}
	
	public Integer getEdad() {
		return edad;
	}
	
	public void setEdad(Integer edad) {
		this.edad = edad;
	}
	
	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	@Override
	public String toString() {
		return "." + this.getDni() + " - " + this.getApellido() + "(" + this.getEdad() + ")";
	}

	@Override
	public int compareTo(Persona p) {
		return this.getDni().compareTo(p.getDni());
	}
}
