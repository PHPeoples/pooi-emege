package listaPersonas;

import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {
		ArrayList<Persona> lista = new ArrayList<>();
		lista.add(new Persona(37000000, "Lopez", 25));
		lista.add(new Persona(10000000, "Perez", 80));
		lista.add(new Persona(40000000, "Gomez", 22));
		lista.add(new Persona(20000000, "Garcia", 49));
		
		System.out.println("Lista original");
		Lista.listarPersonas(lista);
		
		System.out.println("Mayores de 30");
		for (Persona persona : Lista.mayoresDeEdad(lista, 30)) {
			System.out.println(persona);
		}
		
		System.out.println("Lista ordenada por DNI");
		for (Persona persona : Lista.ordenada(lista)) {
			System.out.println(persona);
		}
	}

}
