package listaPersonas;

import java.util.ArrayList;

public class Lista {
	public static void listarPersonas(ArrayList<Persona> L) {
		for (Persona persona : L) {
			System.out.println(persona);
		}
	}
	
	public static ArrayList<Persona> mayoresDeEdad(ArrayList<Persona> L, Integer edad) {
		ArrayList<Persona> mayores = new ArrayList<>();
		
		for (Persona persona : L) {
			if (persona.getEdad() >= edad)
				mayores.add(persona);
		}
		
		return mayores;
	}
	
	public static ArrayList<Persona> ordenada(ArrayList<Persona> L) {
		L.sort(null);
		return L;
	}
}
