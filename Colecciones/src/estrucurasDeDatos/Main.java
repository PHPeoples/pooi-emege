package estrucurasDeDatos;

public class Main {

	public static void main(String[] args) {
		Pila p = new Pila();
		System.out.println(p.isVacia());
		
		p.poner("Greco");
		System.out.println(p.ver());
		System.out.println(p.isVacia());
		
		p.poner("David");
		System.out.println(p.ver());
		System.out.println(p.isVacia());
		
		p.poner("Matias");
		System.out.println(p.ver());
		System.out.println(p.isVacia());
		
		/*
		p.sacar();
		System.out.println(p.isVacia());
		
		p.sacar();
		System.out.println(p.isVacia());
		*/
		
		System.out.println(p.toString());
	}

}
