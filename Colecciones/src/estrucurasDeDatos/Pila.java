package estrucurasDeDatos;

public class Pila {
	class Nodo {
		private String info;
		private Nodo sig;
		
		public Nodo(String info, Nodo sig) {
			this.setInfo(info);
			this.setSig(sig);
		}
		
		public Nodo(String info) {
			this(info, null);
		}
		
		public String getInfo() {
			return info;
		}
		
		public void setInfo(String info) {
			this.info = info;
		}
		
		public Nodo getSig() {
			return sig;
		}
		
		public void setSig(Nodo sig) {
			this.sig = sig;
		}
	}
	
	private Nodo tope;
	
	public Pila() {
		this.setTope(null);
	}

	public Nodo getTope() {
		return tope;
	}

	public void setTope(Nodo tope) {
		this.tope = tope;
	}
	
	public void poner(String info) {
		this.setTope(new Nodo(info, this.getTope()));
	}
	
	public String ver() {
		return this.getTope().getInfo();
	}
	
	public void sacar() {
		this.setTope(this.getTope().getSig());
	}
	
	public Boolean isVacia() {
		return this.getTope() == null;
	}
	
	@Override
	public String toString() {
		String representacion = "";
		Pila pilaAux = new Pila();
		
		while (!this.isVacia()) {
			String dato = this.ver();
			pilaAux.poner(dato);
			this.sacar();
			
			representacion += dato + " ";
		}
		
		while (!pilaAux.isVacia()) {
			this.poner(pilaAux.ver());
			pilaAux.sacar();
		}
		
		return representacion;
	}
}
