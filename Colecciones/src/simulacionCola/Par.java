package simulacionCola;

public class Par {
	private Integer posicion;
	private Integer productos;
	
	public Par(Integer posicion, Integer productos) {
		this.setPosicion(posicion);
		this.setProductos(productos);
	}
	
	public Par(Integer i) {
		this(i, i);
	}
	
	public Par() {
		this(0, 0);
	}
	
	public Integer getPosicion() {
		return posicion;
	}
	
	public void setPosicion(Integer posicion) {
		this.posicion = posicion;
	}
	
	public Integer getProductos() {
		return productos;
	}
	
	public void setProductos(Integer productos) {
		this.productos = productos;
	}
	
	@Override
	public String toString() {
		return "[" + this.getPosicion() + " " + this.getProductos() + "]";
	}
}
