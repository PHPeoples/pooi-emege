package simulacionCola;

import java.util.concurrent.ConcurrentLinkedQueue;

public class SimulacionCola {
	
	public static void main(String[] args) {
		ConcurrentLinkedQueue<Par> colaA = new ConcurrentLinkedQueue<>();
		ConcurrentLinkedQueue<Par> colaB = new ConcurrentLinkedQueue<>();
		
		colaA.offer(new Par(1, 3));
		colaA.offer(new Par(2, 8));
		colaA.offer(new Par(3, 4));
		colaA.offer(new Par(4, 15));
		colaA.offer(new Par(5, 1));
		colaA.offer(new Par(6, 2));
		colaA.offer(new Par(7, 6));
		colaA.offer(new Par(8, 5));
		colaA.offer(new Par(9, 10));
		colaA.offer(new Par(10, 3));
		
		for (Par par : colaA) {
			if (par.getProductos() < 5) {
				colaB.offer(par);
				colaA.remove(par);
			}
		}
		
		System.out.println("Cola A:");
		for (Par par : colaA) {
			System.out.println(par);
		}
		
		System.out.println("Cola B:");
		for (Par par : colaB) {
			System.out.println(par);
		}
	}

}
