package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;

import invertirCadenaStack.UsoStack;

class Test {

	@AfterEach
	void tearDown() throws Exception {
	}

	@org.junit.jupiter.api.Test
	void invertirCadena() {
		assertEquals("nemaxE", UsoStack.invertirCadena("Examen"));
	}

}
