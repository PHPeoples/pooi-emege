package colecciones;

import java.util.ArrayList;

public class UsoArrayList {

	public static void main(String[] args) {
		//Por defecto la capacidad es 10. Va creciendo dinmámicamente duplicando su capacidad
		//Se puede setear su capacidad dentro del constructor
		//Si se tiene un estimado de cantidad de elementos es mas performante crearlo con esa capacidad para evitar que crezca automaticamente
		//Capacidad =! tamaño
		//El tamaño es la cantidad de elementos que tiene
		ArrayList<Integer> vector = new ArrayList<Integer>();
		System.out.println("Esta vacio?: " + vector.isEmpty());
		vector.add(2);
		vector.add(5);
		vector.add(3);
		System.out.println("toString: " + vector);
		//remove elimina la posicion, no el dato
		vector.remove(2);
		System.out.println("toString: " + vector);
		System.out.println("Esta vacio?: " + vector.isEmpty());
		System.out.println("Posición del elemento 5: " + vector.indexOf(5));
		System.out.println("Valor del indice 0: " + vector.get(0));
		System.out.println("Tamaño del vector: " + vector.size());
	}

}
